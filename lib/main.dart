import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:reminder_app/app.dart';
import 'package:reminder_app/core/app_bloc_observer.dart';
import 'package:reminder_app/core/common/common_colors.dart';
import 'package:reminder_app/core/di/service_locator.dart';
import 'package:timezone/data/latest_all.dart' as tz;

@pragma('vm:entry-point')
void notificationTapBackground(NotificationResponse notificationResponse) {
  if (notificationResponse.input?.isNotEmpty ?? false) {}
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: CommonColors.transparent,
    statusBarBrightness: Brightness.light,
  ));
  Intl.defaultLocale = 'id_ID';
  initializeDateFormatting();
  tz.initializeTimeZones();

  Bloc.observer = AppBlocObserver();
  await setupServiceLocator();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.requestNotificationsPermission();

  runApp(const MainApp());
}
