import 'package:equatable/equatable.dart';

class Reminder extends Equatable {
  final int? id;
  final String? name;
  final String? desc;
  final DateTime? dateTime;
  final String? url;
  bool isActivated;
  final bool? isDeleted;
  final bool? withLocation;
  final double? lat;
  final double? long;
  final int? priority;

  Reminder({
    required this.id,
    required this.name,
    required this.desc,
    required this.dateTime,
    required this.url,
    this.isActivated = false,
    required this.isDeleted,
    required this.withLocation,
    required this.lat,
    required this.long,
    required this.priority,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        desc,
        dateTime,
        url,
        isActivated,
        isDeleted,
        withLocation,
        lat,
        long,
        priority,
      ];
}
