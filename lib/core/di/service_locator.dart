import 'package:get_it/get_it.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:reminder_app/core/app_constants.dart';
import 'package:reminder_app/data/local/datasource/reminder_local_data_source.dart';
import 'package:reminder_app/data/repositories/reminder/reminder_repository.dart';
import 'package:reminder_app/data/repositories/reminder/reminder_repository_impl.dart';
import 'package:reminder_app/modules/main/bloc/main_bloc.dart';
import 'package:reminder_app/modules/main/usecase/get_list_reminder_usecase.dart';
import 'package:reminder_app/modules/reminder_detail/bloc/reminder_detail_bloc.dart';
import 'package:reminder_app/modules/reminder_detail/usecase/add_reminder_usecase.dart';
import 'package:reminder_app/modules/reminder_detail/usecase/get_reminder_by_id_usecase.dart';

final serviceLocator = GetIt.instance;

setupServiceLocator() async {
  //hive
  await Hive.initFlutter();
  await Hive.openBox<String>(AppConstants.reminderBoxName);

  //datasource
  serviceLocator.registerFactory<ReminderLocalDataSource>(
      () => ReminderLocalDataSourceImpl(boxName: AppConstants.reminderBoxName));

  //repositories
  serviceLocator.registerLazySingleton<ReminderRepository>(
      () => ReminderRepositoryImpl(dataSource: serviceLocator()));

  //usecase
  serviceLocator.registerLazySingleton<GetListReminderUseCase>(
      () => GetListReminderUseCase(serviceLocator()));
  serviceLocator.registerLazySingleton<GetReminderByIdUseCase>(
      () => GetReminderByIdUseCase(serviceLocator()));
  serviceLocator.registerLazySingleton<AddReminderUseCase>(
      () => AddReminderUseCase(serviceLocator()));

  //bloc
  serviceLocator.registerFactory<MainBloc>(
      () => MainBloc(serviceLocator(), serviceLocator()));
  serviceLocator.registerFactory<ReminderDetailBloc>(() =>
      ReminderDetailBloc(serviceLocator(), serviceLocator(), serviceLocator()));
}
