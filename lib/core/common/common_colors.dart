import 'package:flutter/material.dart';

class CommonColors {
  static const primaryColor = Color(0xff006769);
  static const secondaryColor = Color(0xff40A578);
  static const accentColor = Color(0xff9DDE8B);

  static const bgColor = Color(0xff222831);
  static const textColorLight = Colors.black;
  static const textColorDark = Colors.white;

  static const transparent = Colors.transparent;
}
