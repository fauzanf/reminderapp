import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:reminder_app/core/common/common_colors.dart';

class CommonTheme {
  static ThemeData themeData = ThemeData(
    appBarTheme: CommonTheme.appBarTheme,
    scaffoldBackgroundColor: CommonColors.bgColor,
    textTheme: GoogleFonts.montserratTextTheme(),
  );
  static AppBarTheme appBarTheme =
      const AppBarTheme(backgroundColor: CommonColors.bgColor);
  static final TextStyle fHeading1 = GoogleFonts.urbanist(
      fontSize: 40,
      color: CommonColors.textColorDark,
      fontWeight: FontWeight.w400);
  static final TextStyle fHeading2 = GoogleFonts.urbanist(
      fontSize: 28,
      color: CommonColors.textColorDark,
      fontWeight: FontWeight.w400);
  static final TextStyle fHeading3 = GoogleFonts.urbanist(
      fontSize: 22,
      color: CommonColors.textColorDark,
      fontWeight: FontWeight.w400);
  static final TextStyle fHeading4 = GoogleFonts.urbanist(
      fontSize: 20,
      color: CommonColors.textColorDark,
      fontWeight: FontWeight.w400);
  static final TextStyle fHeading5 = GoogleFonts.urbanist(
      fontSize: 18,
      color: CommonColors.textColorDark,
      fontWeight: FontWeight.w400);

  static final TextStyle bodyLarge = GoogleFonts.urbanist(
      fontSize: 16,
      color: CommonColors.textColorDark,
      fontWeight: FontWeight.w400);
  static final TextStyle bodyMedium = GoogleFonts.urbanist(
      fontSize: 14,
      color: CommonColors.textColorDark,
      fontWeight: FontWeight.w400);
}
