
class HiveException implements Exception {
  final String msg;

  HiveException(this.msg);

  @override
  String toString() {
    return msg;
  }

}