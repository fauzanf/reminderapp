import 'package:flutter/material.dart';
import 'package:reminder_app/core/app_constants.dart';
import 'package:reminder_app/core/common/common_colors.dart';
import 'package:reminder_app/core/common/common_theme.dart';

class CommonDialog {
  void showConfirmationDialog(
    BuildContext context, {
    String title = 'Info',
    String description = '',
    VoidCallback? onLeftBtnClick,
    VoidCallback? onRightBtnClick,
    String btnTextLeft = 'Tidak',
    String btnTextRight = 'Ya',
    Color descriptionColor = CommonColors.textColorLight,
    FontWeight descriptionFontWeight = FontWeight.normal,
    bool onlyRightBtn = false,
  }) {
    Dialog dialog = Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(AppConstants.radiusLarge)),
      child: Container(
        height: 250.0,
        width: double.infinity,
        color: Colors.white,
        padding: const EdgeInsets.all(AppConstants.paddingNormal),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: [
                Text(
                  title,
                  style: CommonTheme.fHeading3.copyWith(color: Colors.black),
                ),
                const SizedBox(height: AppConstants.paddingLarge),
                Text(
                  description,
                  style: CommonTheme.fHeading4.copyWith(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12, right: 6),
                    child: MaterialButton(
                      onPressed: onLeftBtnClick,
                      color: CommonColors.primaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4)),
                      child: Text(
                        btnTextLeft,
                        style: CommonTheme.bodyMedium
                            .copyWith(color: Colors.white),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 6, right: 12),
                    child: MaterialButton(
                      onPressed: onRightBtnClick,
                      color: CommonColors.primaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4)),
                      child: Text(
                        btnTextRight,
                        style: CommonTheme.bodyMedium
                            .copyWith(color: Colors.white),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );

    showDialog(context: context, builder: (BuildContext context) => dialog);
  }
}
