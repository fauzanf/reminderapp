import 'package:flutter/material.dart';
import 'package:reminder_app/core/common/common_theme.dart';

class CommonTextField extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final String errorMsg;
  final int minLines;
  final int maxLines;

  const CommonTextField({
    super.key,
    required this.controller,
    required this.hintText,
    required this.errorMsg,
    this.minLines = 1,
    this.maxLines = 1,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      style: CommonTheme.fHeading5,
      cursorColor: Colors.white,
      minLines: minLines,
      maxLines: maxLines,
      decoration: InputDecoration(
        focusColor: Colors.white,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: CommonTheme.fHeading5.copyWith(
          color: Colors.grey,
        ),
        errorBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
        ),
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return errorMsg;
        }

        return null;
      },
    );
  }
}
