import 'package:reminder_app/core/model/enums/common_error_type.dart';

class CommonError {
  CommonErrorType errorType;
  Exception? error;
  String? message;
  bool isShowErrorView;
  CommonError({
    this.errorType = CommonErrorType.unknownException,
    this.error,
    this.message,
    this.isShowErrorView = true,
  });
}
