class AppConstants {
  static const String appName = "Reminder App";
  static const String noData = "Tidak ada data";
  static const String completedReminder = "Semua reminder telah selesai";
  static const String reminderBoxName = "reminderBox";
  static const String reminder = "Reminder";
  static const String detail = "Detail";
  static const String edit = "Ubah";
  static const String add = "Tambah";
  static const String name = "Nama";
  static const String desc = "Deskripsi";
  static const String formRequired = "harus diisi";
  static const String date = "Tanggal";
  static const String location = "Lokasi";
  static const String set = "Atur";
  static const String priority = "Prioritas";
  static const String successAddReminder = "Berhasil menambahkan reminder";
  static const String errorAddReminder = "Gagal menambahlan reminder";
  static const String reminderDone = "Reminder telah selesai";
  static const String deleteSuccess = "Reminder berhasil dihapus";
  static const String descDelete =
      "Apakah kamu yakin ingin menghapus reminder ini ? ";
  static const String activateLocReminder = "Aktifkan Pengingat lokasi";
  static const String permissionDenied = "Izin layanan lokasi ditolak";
  static const String serviceDisabled = "Layanan lokasi tidak aktif.";
  static const String permanentlyDenied =
      "Izin layanan lokasi ditolak secara permanen.";

  //routes
  static const String mainPage = "/";
  static const String detailReminderPage = "detail-reminder";

  static const double paddingExtraSmall = 4.0;
  static const double paddingSmall = 8.0;
  static const double paddingMedium = 12.0;
  static const double paddingNormal = 16.0;
  static const double paddingLarge = 24.0;
  static const double paddingExtraLarge = 40.0;

  //radius
  static const double radiusSmall = 4.0;
  static const double radiusNormal = 8.0;
  static const double radiusLarge = 12.0;
  static const double radiusExtraLarge = 16.0;

  //icon size
  static const double iconSmall = 16.0;
  static const double iconNormal = 24.0;
  static const double iconExtraLarge = 40.0;

//spacing
  static const double spacingSmall = 2.0;
  static const double spacingMedium = 6.0;
}
