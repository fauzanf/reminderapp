import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:reminder_app/core/app_constants.dart';
import 'package:reminder_app/core/di/service_locator.dart';
import 'package:reminder_app/modules/main/bloc/main_bloc.dart';
import 'package:reminder_app/modules/main/views/main_view.dart';
import 'package:reminder_app/modules/reminder_detail/bloc/reminder_detail_bloc.dart';
import 'package:reminder_app/modules/reminder_detail/views/reminder_detail_view.dart';

class AppRouter {
  GoRouter generateRoute() {
    return GoRouter(
      routes: [
        GoRoute(
          path: AppConstants.mainPage,
          builder: (context, state) {
            return BlocProvider.value(
              value: serviceLocator<MainBloc>()
                ..add(
                  MainGetRecentReminderEvent(),
                ),
              child: const MainView(),
            );
          },
          routes: <RouteBase>[
            GoRoute(
              path: AppConstants.detailReminderPage,
              builder: (context, state) {
                final id = state.extra! as int;
                return BlocProvider(
                  create: (_) => serviceLocator<ReminderDetailBloc>(),
                  child: ReminderDetailView(
                    id: id,
                  ),
                );
              },
            ),
          ],
        ),
      ],
    );
  }
}
