import 'package:intl/intl.dart';

class DateTimeHelper {
  String formatText(DateTime dateTime) {
    return DateFormat('yyyy-MM-dd HH:mm').format(dateTime);
  }
}
