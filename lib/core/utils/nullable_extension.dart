extension NullableString on String? {
  String safe() {
    return this ?? '';
  }

  bool isNullOrEmpty() {
    return this == null || this == '';
  }
}

extension NullableInt on int? {
  int safe() {
    return this ?? 0;
  }

  bool isNullOrZero() {
    return this == null || this == 0;
  }
}

extension NullableDouble on double? {
  double safe() {
    return this ?? 0.0;
  }

  bool isNullOrZero() {
    return this == null || this == 0;
  }
}

extension NullableList<T> on List<T>? {
  List<T> safe() {
    return this ?? <T>[];
  }
}

extension NullableBoolean on bool? {
  bool safe() {
    return this ?? false;
  }
}

extension NullableDatetime on DateTime? {
  DateTime safe() {
    return this ?? DateTime.now();
  }
}
