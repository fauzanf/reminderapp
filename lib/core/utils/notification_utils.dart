import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'dart:math';

import 'package:reminder_app/main.dart';
import 'package:timezone/timezone.dart' as tz;

class NotificationUtils {
  var random = Random();

  AndroidNotificationChannel channel = const AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    description: 'This channel is used for important notifications.',
    importance: Importance.max,
    enableVibration: true,
    playSound: true,
  );
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  InitializationSettings initializationSettings() {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    const DarwinInitializationSettings initializationSettingsDarwin =
        DarwinInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
    );

    return const InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsDarwin,
    );
  }

  showNotif({
    ReceiveNotification? notification,
  }) async {
    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings(),
      onDidReceiveBackgroundNotificationResponse: notificationTapBackground,
    );
    tz.timeZoneDatabase.locations;
    final location = tz.getLocation("Asia/Jakarta");

    if (notification != null) {
      flutterLocalNotificationsPlugin.zonedSchedule(
        notification.id!,
        notification.title,
        notification.body,
        tz.TZDateTime.from(notification.dateTime!, location),
        NotificationDetails(
          android: AndroidNotificationDetails(
            channel.id,
            channel.name,
            channelDescription: channel.description,
            importance: channel.importance,
            priority: notification.priority!,
            playSound: channel.playSound,
          ),
          iOS: const DarwinNotificationDetails(
            presentAlert: true,
            presentBadge: true,
            presentSound: true,
          ),
        ),
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
      );
    }
  }

  cancelNotif(int id) {
    flutterLocalNotificationsPlugin.cancel(id);
  }
}

class ReceiveNotification {
  ReceiveNotification({
    this.id,
    this.title,
    this.body,
    this.payload,
    this.priority,
    this.dateTime,
  });

  final int? id;
  final String? title;
  final String? body;
  final String? payload;
  final Priority? priority;
  final DateTime? dateTime;
}
