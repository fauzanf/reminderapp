import 'package:geolocator/geolocator.dart';
import 'package:reminder_app/core/app_constants.dart';

Future<Position> getCurrentLocation() async {
  final service = await Geolocator.isLocationServiceEnabled();

  if (service == false) {
    return Future.error(AppConstants.serviceDisabled);
  }

  LocationPermission permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
  }

  if (permission == LocationPermission.denied) {
    return Future.error(AppConstants.permissionDenied);
  }

  if (permission == LocationPermission.deniedForever) {
    return Future.error(AppConstants.permanentlyDenied);
  }

  final position = await Geolocator.getCurrentPosition(
    desiredAccuracy: LocationAccuracy.best,
    timeLimit: const Duration(seconds: 20),
  );

  return position;
}
