import 'package:hive/hive.dart';
import 'package:reminder_app/core/model/reminder/reminder.dart';
import 'package:reminder_app/core/utils/nullable_extension.dart';

@HiveType(typeId: 0)
class ReminderModel extends HiveObject {
  @HiveField(0)
  int id;

  @HiveField(1)
  String name;

  @HiveField(2)
  String desc;

  @HiveField(3)
  DateTime dateTime;

  @HiveField(4)
  String url;

  @HiveField(5)
  bool isActivated;

  @HiveField(6)
  bool isDeleted;

  @HiveField(7)
  bool withLocation;

  @HiveField(8)
  double lat;

  @HiveField(9)
  double long;

  @HiveField(10)
  int priority;

  ReminderModel({
    required this.id,
    required this.name,
    required this.desc,
    required this.dateTime,
    required this.url,
    required this.isActivated,
    required this.isDeleted,
    required this.withLocation,
    required this.lat,
    required this.long,
    required this.priority,
  });

  factory ReminderModel.fromJson(Map<String, dynamic> json) => ReminderModel(
        id: json["id"],
        name: json["name"],
        desc: json["desc"],
        dateTime: DateTime.parse(json["dateTime"]),
        url: json["url"],
        isActivated: json["isActivated"],
        isDeleted: json["isDeleted"],
        withLocation: json["withLocation"],
        lat: json["lat"],
        long: json["long"],
        priority: json["priority"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "desc": desc,
        "dateTime": dateTime.toIso8601String(),
        "url": url,
        "isActivated": isActivated,
        "isDeleted": isDeleted,
        "withLocation": withLocation,
        "lat": lat,
        "long": long,
        "priority": priority,
      };

  Reminder toEntity() => Reminder(
        id: id.safe(),
        name: name.safe(),
        desc: desc.safe(),
        dateTime: dateTime,
        url: url.safe(),
        isActivated: isActivated.safe(),
        isDeleted: isDeleted.safe(),
        withLocation: withLocation.safe(),
        lat: lat.safe(),
        long: long.safe(),
        priority: priority.safe(),
      );
}
