import 'dart:convert';

import 'package:hive/hive.dart';
import 'package:reminder_app/core/app_constants.dart';
import 'package:reminder_app/core/common/exception.dart';
import 'package:reminder_app/data/local/model/reminder_model.dart';

abstract class ReminderLocalDataSource {
  Future<void> createOrUpdateReminder(ReminderModel reminderModel);

  Future<void> deleteReminder(int id);

  Future<List<ReminderModel>> getReminderList();

  Future<ReminderModel> getReminderListById(int id);
}

class ReminderLocalDataSourceImpl implements ReminderLocalDataSource {
  final String boxName;

  ReminderLocalDataSourceImpl({required this.boxName});

  @override
  Future<void> createOrUpdateReminder(ReminderModel reminderModel) async {
    await Hive.box<String>(boxName)
        .put(reminderModel.id, jsonEncode(reminderModel));
  }

  @override
  Future<void> deleteReminder(int id) async {
    await Hive.box<String>(boxName).delete(id);
  }

  @override
  Future<List<ReminderModel>> getReminderList() async {
    final data = Hive.box<String>(boxName).values.map((e) {
      return ReminderModel.fromJson(jsonDecode(e));
    }).toList();
    if (data.isEmpty) throw HiveException(AppConstants.noData);
    return data;
  }

  @override
  Future<ReminderModel> getReminderListById(int id) async {
    final dataLocal = Hive.box<String>(boxName).get(id);
    if (dataLocal == null) throw HiveException(AppConstants.noData);

    return ReminderModel.fromJson(jsonDecode(dataLocal));
  }
}
