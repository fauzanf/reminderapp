
import 'package:either_dart/either.dart';
import 'package:reminder_app/core/common/common_error.dart';
import 'package:reminder_app/core/common/exception.dart';
import 'package:reminder_app/core/model/reminder/reminder.dart';
import 'package:reminder_app/data/local/datasource/reminder_local_data_source.dart';
import 'package:reminder_app/data/local/model/reminder_model.dart';
import 'package:reminder_app/data/repositories/reminder/reminder_repository.dart';

class ReminderRepositoryImpl implements ReminderRepository {
  final ReminderLocalDataSource dataSource;

  ReminderRepositoryImpl({required this.dataSource});

  @override
  Future<void> createOrUpdateReminder(ReminderModel reminderModel) async {
    dataSource.createOrUpdateReminder(reminderModel);
  }

  @override
  Future<void> deleteReminder(int id) async {
    dataSource.deleteReminder(id);
  }

  @override
  Future<Either<CommonError, List<Reminder>>> getReminderList() async {
    try {
      final res = await dataSource.getReminderList();
      List<Reminder> list = [];

      for (var element in res) {
        list.add(element.toEntity());
      }
      return Right(list);
    } on HiveException catch (e) {
      return Left(CommonError(error: e, message: e.msg));
    }
  }

  @override
  Future<Either<CommonError, Reminder>> getReminderListById(int id) async {
    try {
      final res = await dataSource.getReminderListById(id);

      return Right(res.toEntity());
    } on HiveException catch (e) {
      return Left(CommonError(error: e, message: e.msg));
    }
  }
}
