import 'package:either_dart/either.dart';
import 'package:reminder_app/core/common/common_error.dart';
import 'package:reminder_app/core/model/reminder/reminder.dart';
import 'package:reminder_app/data/local/model/reminder_model.dart';

abstract class ReminderRepository {
  Future<void> createOrUpdateReminder(ReminderModel reminderModel);

  Future<void> deleteReminder(int id);

  Future<Either<CommonError, List<Reminder>>> getReminderList();

  Future<Either<CommonError, Reminder>> getReminderListById(int id);
}
