part of 'reminder_detail_bloc.dart';

sealed class ReminderDetailState extends Equatable {
  const ReminderDetailState();

  @override
  List<Object> get props => [];
}

final class ReminderDetailInitial extends ReminderDetailState {
  final int lengthList;

  const ReminderDetailInitial({required this.lengthList});

  @override
  List<Object> get props => [lengthList];
}

final class ReminderDetailByIdLoadingState extends ReminderDetailState {}

final class ReminderDetailByIdErrorState extends ReminderDetailState {}

final class ReminderDetailByIdSuccessState extends ReminderDetailState {
  final Reminder reminder;

  const ReminderDetailByIdSuccessState({required this.reminder});

  @override
  List<Object> get props => [reminder];
}

final class AddReminderLoadingState extends ReminderDetailState {}

final class AddReminderErrorState extends ReminderDetailState {}

final class AddReminderSuccessState extends ReminderDetailState {}