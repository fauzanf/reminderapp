part of 'reminder_detail_bloc.dart';

sealed class ReminderDetailEvent extends Equatable {
  const ReminderDetailEvent();

  @override
  List<Object> get props => [];
}

class ReminderInitEvent extends ReminderDetailEvent {
  final bool isEdit;
  final int id;

  const ReminderInitEvent({required this.isEdit, required this.id});

  @override
  List<Object> get props => [isEdit, id];
}

class ReminderDetailSubmitEvent extends ReminderDetailEvent {
  final ReminderModel reminderModel;

  const ReminderDetailSubmitEvent({required this.reminderModel});

  @override
  List<Object> get props => [reminderModel];
}
