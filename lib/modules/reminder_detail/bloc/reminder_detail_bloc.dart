import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:reminder_app/core/model/reminder/reminder.dart';
import 'package:reminder_app/core/utils/notification_utils.dart';
import 'package:reminder_app/data/local/model/reminder_model.dart';
import 'package:reminder_app/modules/main/usecase/get_list_reminder_usecase.dart';
import 'package:reminder_app/modules/reminder_detail/usecase/add_reminder_usecase.dart';
import 'package:reminder_app/modules/reminder_detail/usecase/get_reminder_by_id_usecase.dart';

part 'reminder_detail_event.dart';
part 'reminder_detail_state.dart';

class ReminderDetailBloc
    extends Bloc<ReminderDetailEvent, ReminderDetailState> {
  final GetReminderByIdUseCase _getReminderByIdUseCase;
  final AddReminderUseCase _addReminderUseCase;
  final GetListReminderUseCase _getListReminderUseCase;

  ReminderDetailBloc(
    this._getReminderByIdUseCase,
    this._addReminderUseCase,
    this._getListReminderUseCase,
  ) : super(const ReminderDetailInitial(lengthList: 0)) {
    on<ReminderDetailEvent>(
      (event, emit) async {
        if (event is ReminderInitEvent) {
          await initReminder(event, emit);
        }

        if (event is ReminderDetailSubmitEvent) {
          await addReminder(event, emit);
        }
      },
    );
  }

  FutureOr<void> initReminder(
      ReminderInitEvent event, Emitter<ReminderDetailState> emit) async {
    try {
      if (event.isEdit) {
        emit(ReminderDetailByIdLoadingState());
        final res = await _getReminderByIdUseCase.execute(event.id);

        res.fold(
          (failure) => emit(ReminderDetailByIdErrorState()),
          (data) => emit(ReminderDetailByIdSuccessState(reminder: data)),
        );

        return;
      }

      final resList = await _getListReminderUseCase.execute();
      resList.fold(
        (failure) => emit(ReminderDetailByIdErrorState()),
        (data) => emit(ReminderDetailInitial(lengthList: data.length)),
      );
    } catch (e) {
      emit(ReminderDetailByIdErrorState());
    }
  }

  FutureOr<void> addReminder(ReminderDetailSubmitEvent event,
      Emitter<ReminderDetailState> emit) async {
    emit(AddReminderLoadingState());
    try {
      await _addReminderUseCase.execute(event.reminderModel);
      NotificationUtils().cancelNotif(event.reminderModel.id);
      NotificationUtils().showNotif(
        notification: ReceiveNotification(
          id: event.reminderModel.id,
          title: event.reminderModel.name,
          body: event.reminderModel.desc,
          dateTime: event.reminderModel.dateTime,
          priority: Priority.values[event.reminderModel.priority],
          payload: "",
        ),
      );

      emit(AddReminderSuccessState());
    } catch (e) {
      emit(AddReminderErrorState());
    }
  }
}
