import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker_plus/flutter_datetime_picker_plus.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'package:go_router/go_router.dart';
import 'package:latlong2/latlong.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:reminder_app/core/app_constants.dart';
import 'package:reminder_app/core/common/common_colors.dart';
import 'package:reminder_app/core/common/common_theme.dart';
import 'package:reminder_app/core/common/widgets/common_text_field.dart';
import 'package:reminder_app/core/utils/date_time_helper.dart';
import 'package:reminder_app/core/utils/location_utils.dart';
import 'package:reminder_app/data/local/model/reminder_model.dart';
import 'package:reminder_app/modules/reminder_detail/bloc/reminder_detail_bloc.dart';

class ReminderDetailView extends StatefulWidget {
  final int id;
  const ReminderDetailView({super.key, required this.id});

  @override
  State<ReminderDetailView> createState() => _ReminderDetailViewState();
}

class _ReminderDetailViewState extends State<ReminderDetailView> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _nameEditing = TextEditingController();
  final TextEditingController _descEditing = TextEditingController();
  List<String> list = <String>['Low', 'Medium', 'High'];
  String selectedPriority = "Low";
  DateTime selectedTime = DateTime.now();
  int id = 0;
  bool withLocation = false;
  MapController _mapController = MapController();

  @override
  void initState() {
    _mapController = MapController();
    _mapController.mapEventStream.listen((event) async {
      if (event is MapEventMoveEnd) {}
    });
    context.read<ReminderDetailBloc>().add(
          ReminderInitEvent(id: widget.id, isEdit: widget.id >= 0),
        );
    super.initState();
  }

  @override
  void dispose() async {
    _mapController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CommonColors.transparent,
        iconTheme: const IconThemeData(color: CommonColors.accentColor),
        title: Text(
          widget.id >= 0
              ? "${AppConstants.detail} ${AppConstants.reminder}"
              : "${AppConstants.add} ${AppConstants.reminder}",
          style: CommonTheme.fHeading3.copyWith(
            color: CommonColors.accentColor,
          ),
        ),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return BlocListener<ReminderDetailBloc, ReminderDetailState>(
      listener: (context, state) {
        if (state is ReminderDetailInitial) {
          if (widget.id < 0) {
            id = state.lengthList;
          }
        }

        if (state is ReminderDetailByIdSuccessState) {
          setState(() {
            _nameEditing.text = state.reminder.name ?? "";
            _descEditing.text = state.reminder.desc ?? "";
            selectedTime = state.reminder.dateTime ?? DateTime.now();
            selectedPriority = list[state.reminder.priority ?? 0];
          });
        }

        if (state is AddReminderSuccessState) {
          final snackBar = SnackBar(
            content: Text(
              AppConstants.successAddReminder,
              style: CommonTheme.bodyMedium.copyWith(color: Colors.black),
            ),
            backgroundColor: Colors.lightGreen,
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          context.go("/");
        }

        if (state is AddReminderErrorState) {
          final snackBar = SnackBar(
            content: Text(
              AppConstants.errorAddReminder,
              style: CommonTheme.bodyMedium.copyWith(color: Colors.white),
            ),
            backgroundColor: Colors.redAccent,
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      },
      child: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView(
                padding: const EdgeInsets.all(AppConstants.paddingNormal),
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        Container(
                          padding:
                              const EdgeInsets.all(AppConstants.paddingNormal),
                          decoration: BoxDecoration(
                              color: Colors.grey[900],
                              borderRadius: BorderRadius.circular(
                                  AppConstants.radiusNormal)),
                          child: Column(
                            children: [
                              CommonTextField(
                                controller: _nameEditing,
                                hintText: AppConstants.name,
                                errorMsg:
                                    "${AppConstants.name} ${AppConstants.formRequired}",
                              ),
                              const SizedBox(
                                height: AppConstants.paddingNormal,
                              ),
                              CommonTextField(
                                controller: _descEditing,
                                minLines: 3,
                                maxLines: 3,
                                hintText: AppConstants.desc,
                                errorMsg:
                                    "${AppConstants.desc} ${AppConstants.formRequired}",
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: AppConstants.paddingNormal),
                        Container(
                          padding:
                              const EdgeInsets.all(AppConstants.paddingNormal),
                          decoration: BoxDecoration(
                              color: Colors.grey[900],
                              borderRadius: BorderRadius.circular(
                                  AppConstants.radiusNormal)),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      const Icon(
                                        Icons.date_range_rounded,
                                        color: Colors.white,
                                      ),
                                      const SizedBox(
                                          width: AppConstants.paddingSmall),
                                      Text(
                                        AppConstants.date,
                                        style: CommonTheme.fHeading5
                                            .copyWith(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        selectedTime == DateTime.now()
                                            ? AppConstants.set
                                            : DateTimeHelper()
                                                .formatText(selectedTime),
                                        style: CommonTheme.bodyMedium
                                            .copyWith(color: Colors.white),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          DatePicker.showDateTimePicker(
                                            context,
                                            showTitleActions: true,
                                            minTime: DateTime(2000, 1, 1),
                                            maxTime: DateTime(2500, 1, 1),
                                            onConfirm: (date) {
                                              setState(() {
                                                selectedTime = date;
                                              });
                                            },
                                            currentTime: selectedTime,
                                            locale: LocaleType.id,
                                          );
                                        },
                                        color: Colors.white,
                                        icon: const Icon(Icons.edit),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: AppConstants.paddingNormal,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      const Icon(
                                        Icons.priority_high_rounded,
                                        color: Colors.white,
                                      ),
                                      const SizedBox(
                                          width: AppConstants.paddingSmall),
                                      Text(
                                        AppConstants.priority,
                                        style: CommonTheme.fHeading5
                                            .copyWith(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(
                                        AppConstants.paddingSmall,
                                      ),
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                        horizontal:
                                            AppConstants.paddingExtraSmall,
                                        vertical: 0.0),
                                    child: DropdownButton<String>(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal:
                                              AppConstants.paddingExtraSmall,
                                          vertical: 0.0),
                                      value: selectedPriority,
                                      icon: const Icon(
                                        Icons.keyboard_arrow_down_rounded,
                                        color: Colors.black,
                                      ),
                                      elevation: 16,
                                      style:
                                          const TextStyle(color: Colors.black),
                                      dropdownColor: Colors.white,
                                      underline: Container(
                                        height: 2,
                                        color: Colors.white,
                                      ),
                                      onChanged: (String? value) {
                                        setState(() {
                                          selectedPriority = value!;
                                        });
                                      },
                                      items: list.map<DropdownMenuItem<String>>(
                                          (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                            style:
                                                CommonTheme.bodyMedium.copyWith(
                                              color: Colors.black,
                                            ),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: AppConstants.paddingNormal),
                        Visibility(
                          visible: false,
                          child: Container(
                            padding: const EdgeInsets.all(
                                AppConstants.paddingNormal),
                            decoration: BoxDecoration(
                                color: Colors.grey[900],
                                borderRadius: BorderRadius.circular(
                                    AppConstants.radiusNormal)),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        const Icon(
                                          Icons.location_pin,
                                          color: Colors.white,
                                        ),
                                        const SizedBox(
                                            width: AppConstants.paddingSmall),
                                        Text(
                                          AppConstants.activateLocReminder,
                                          style: CommonTheme.fHeading5
                                              .copyWith(color: Colors.white),
                                        ),
                                      ],
                                    ),
                                    Switch(
                                      value: withLocation,
                                      activeColor: CommonColors.accentColor,
                                      onChanged: (val) async {
                                        if (await Permission.location
                                            .request()
                                            .isGranted) {
                                          setState(() {
                                            withLocation = val;
                                          });
                                        }
                                      },
                                    ),
                                  ],
                                ),
                                Container(
                                  width: double.infinity,
                                  height: withLocation ? 200 : 0,
                                  color: Colors.transparent,
                                  child: FutureBuilder<Position>(
                                    future: getCurrentLocation(),
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        return FlutterMap(
                                          mapController: _mapController,
                                          options: MapOptions(
                                            initialCenter: LatLng(
                                              snapshot.data!.latitude,
                                              snapshot.data!.longitude,
                                            ),
                                            initialZoom: 15.0,
                                            maxZoom: 18,
                                            minZoom: 0,
                                            keepAlive: true,
                                          ),
                                          children: [
                                            TileLayer(
                                              urlTemplate:
                                                  "https://tile.openstreetmap.org/{z}/{x}/{y}.png",
                                            )
                                          ],
                                        );
                                      }

                                      return const SizedBox();
                                    },
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  ReminderModel model = ReminderModel(
                    id: id,
                    name: _nameEditing.value.text,
                    desc: _descEditing.value.text,
                    dateTime: selectedTime,
                    url: "",
                    isActivated: false,
                    isDeleted: false,
                    withLocation: withLocation,
                    lat: 0.0,
                    long: 0.0,
                    priority:
                        list.indexWhere((data) => data == selectedPriority),
                  );
                  context.read<ReminderDetailBloc>().add(
                        ReminderDetailSubmitEvent(
                          reminderModel: model,
                        ),
                      );
                }
              },
              style: const ButtonStyle(
                backgroundColor:
                    WidgetStatePropertyAll(CommonColors.accentColor),
                shape: WidgetStatePropertyAll(
                  ContinuousRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(AppConstants.radiusNormal),
                    ),
                  ),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(AppConstants.paddingSmall),
                child: Text(
                  widget.id >= 0
                      ? "${AppConstants.edit} ${AppConstants.reminder}"
                      : "${AppConstants.add} ${AppConstants.reminder}",
                  style: CommonTheme.fHeading5.copyWith(color: Colors.black),
                ),
              ),
            ),
            const SizedBox(
              height: AppConstants.paddingNormal,
            )
          ],
        ),
      ),
    );
  }
}
