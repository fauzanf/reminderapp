import 'package:either_dart/either.dart';
import 'package:reminder_app/core/common/common_error.dart';
import 'package:reminder_app/core/model/reminder/reminder.dart';
import 'package:reminder_app/data/repositories/reminder/reminder_repository.dart';

class GetReminderByIdUseCase {
  final ReminderRepository repository;

  GetReminderByIdUseCase(this.repository);

  Future<Either<CommonError, Reminder>> execute(id) {
    return repository.getReminderListById(id);
  }
}
