import 'package:reminder_app/data/local/model/reminder_model.dart';
import 'package:reminder_app/data/repositories/reminder/reminder_repository.dart';

class AddReminderUseCase {
  final ReminderRepository repository;

  AddReminderUseCase(this.repository);

  Future<void> execute(ReminderModel reminderModel) {
    return repository.createOrUpdateReminder(reminderModel);
  }
}
