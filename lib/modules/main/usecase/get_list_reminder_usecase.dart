import 'package:either_dart/either.dart';
import 'package:reminder_app/core/common/common_error.dart';
import 'package:reminder_app/core/model/reminder/reminder.dart';
import 'package:reminder_app/data/repositories/reminder/reminder_repository.dart';

class GetListReminderUseCase {
  final ReminderRepository repository;

  GetListReminderUseCase(this.repository);

  Future<Either<CommonError, List<Reminder>>> execute() {
    return repository.getReminderList();
  }
}
