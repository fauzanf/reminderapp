import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:reminder_app/core/app_constants.dart';
import 'package:reminder_app/core/common/common_colors.dart';
import 'package:reminder_app/core/common/common_dialog.dart';
import 'package:reminder_app/core/common/common_theme.dart';
import 'package:reminder_app/data/local/model/reminder_model.dart';
import 'package:reminder_app/modules/main/bloc/main_bloc.dart';

class MainView extends StatefulWidget {
  const MainView({super.key});

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CommonColors.transparent,
        title: Text(
          AppConstants.appName,
          style: CommonTheme.fHeading1.copyWith(
            color: CommonColors.accentColor,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => context.go(
            "${AppConstants.mainPage}${AppConstants.detailReminderPage}",
            extra: -1),
        backgroundColor: CommonColors.accentColor,
        label: const Text("${AppConstants.add} ${AppConstants.reminder}"),
        icon: const Icon(Icons.add),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return SafeArea(
      child: BlocConsumer<MainBloc, MainState>(
        builder: (blocContext, state) {
          if (state is MainGetRecentReminderSuccessState) {
            return ListView.separated(
              padding: const EdgeInsets.only(top: AppConstants.paddingLarge),
              shrinkWrap: true,
              itemBuilder: (listContext, index) {
                var item = state.list[index];

                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Checkbox(
                          value: item.isActivated,
                          onChanged: (val) {
                            setState(() {
                              item.isActivated = !item.isActivated;
                            });
                            ReminderModel reminderModel = ReminderModel(
                              id: item.id!,
                              name: item.name!,
                              desc: item.desc!,
                              dateTime: item.dateTime!,
                              url: item.url!,
                              isActivated: item.isActivated,
                              isDeleted: item.isDeleted!,
                              withLocation: item.withLocation!,
                              lat: item.lat!,
                              long: item.long!,
                              priority: item.priority!,
                            );
                            context.read<MainBloc>().add(
                                  MainUpdateReminderEvent(
                                      reminderModel: reminderModel),
                                );
                            final snackBar = SnackBar(
                              content: Text(
                                AppConstants.reminderDone,
                                style: CommonTheme.bodyMedium
                                    .copyWith(color: Colors.black),
                              ),
                              backgroundColor: Colors.lightGreen,
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          },
                        ),
                        const SizedBox(width: AppConstants.paddingExtraSmall),
                        GestureDetector(
                          onTap: () {
                            context.go(
                              "${AppConstants.mainPage}${AppConstants.detailReminderPage}",
                              extra: item.id,
                            );
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                item.name ?? "",
                                style: CommonTheme.bodyLarge
                                    .copyWith(color: Colors.white),
                              ),
                              Text(
                                item.desc ?? "",
                                style: CommonTheme.bodyMedium
                                    .copyWith(color: Colors.white),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    IconButton(
                      onPressed: () {
                        CommonDialog().showConfirmationDialog(
                          context,
                          description: AppConstants.descDelete,
                          onLeftBtnClick: () => Navigator.pop(context),
                          onRightBtnClick: () {
                            ReminderModel reminderModel = ReminderModel(
                              id: item.id!,
                              name: item.name!,
                              desc: item.desc!,
                              dateTime: item.dateTime!,
                              url: item.url!,
                              isActivated: item.isActivated,
                              isDeleted: true,
                              withLocation: item.withLocation!,
                              lat: item.lat!,
                              long: item.long!,
                              priority: item.priority!,
                            );
                            Navigator.pop(context);
                            context.read<MainBloc>().add(
                                  MainUpdateReminderEvent(
                                      reminderModel: reminderModel),
                                );
                            final snackBar = SnackBar(
                              content: Text(
                                AppConstants.deleteSuccess,
                                style: CommonTheme.bodyMedium
                                    .copyWith(color: Colors.black),
                              ),
                              backgroundColor: Colors.lightGreen,
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                          },
                        );
                      },
                      icon: const Icon(
                        Icons.delete_outline_rounded,
                        color: Colors.red,
                      ),
                    )
                  ],
                );
              },
              separatorBuilder: (context, index) {
                return const Divider(
                  color: Colors.grey,
                  thickness: 1.0,
                  height: AppConstants.paddingSmall,
                );
              },
              itemCount: state.list.length,
            );
          }
          return Center(
            child: Text(
              AppConstants.completedReminder,
              style: CommonTheme.fHeading3,
            ),
          );
        },
        listener: (context, state) {},
      ),
    );
  }
}
