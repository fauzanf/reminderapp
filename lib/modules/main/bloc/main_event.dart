part of 'main_bloc.dart';

class MainEvent extends Equatable {
  const MainEvent();

  @override
  List<Object> get props => [];
}

final class MainGetRecentReminderEvent extends MainEvent {}

final class MainUpdateReminderEvent extends MainEvent {
  final ReminderModel reminderModel;

  const MainUpdateReminderEvent({required this.reminderModel});

  @override
  List<Object> get props => [reminderModel];
}
