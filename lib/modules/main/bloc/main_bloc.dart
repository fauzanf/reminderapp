import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'package:reminder_app/core/app_constants.dart';
import 'package:reminder_app/core/model/reminder/reminder.dart';
import 'package:reminder_app/core/utils/notification_utils.dart';
import 'package:reminder_app/data/local/model/reminder_model.dart';
import 'package:reminder_app/modules/main/usecase/get_list_reminder_usecase.dart';
import 'package:reminder_app/modules/reminder_detail/usecase/add_reminder_usecase.dart';

part 'main_event.dart';
part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final GetListReminderUseCase _getListReminderUseCase;
  final AddReminderUseCase _addReminderUseCase;
  MainBloc(
    this._getListReminderUseCase,
    this._addReminderUseCase,
  ) : super(MainInitial()) {
    on<MainEvent>(
      (event, emit) async {
        if (event is MainGetRecentReminderEvent) {
          await mainGetRecentReminderEvent(event, emit);
        }

        if (event is MainUpdateReminderEvent) {
          await updateReminder(event, emit);
        }
      },
    );
  }

  FutureOr<void> mainGetRecentReminderEvent(
      MainGetRecentReminderEvent event, Emitter<MainState> emit) async {
    emit(MainLoadingState());
    try {
      final res = await _getListReminderUseCase.execute();
      res.fold(
        (failure) => emit(MainErrorState(message: failure.message ?? "")),
        (data) {
          var listData = data
              .where((data) => !data.isActivated && !data.isDeleted!)
              .toList()
              .reversed
              .toList();
          if (listData.isNotEmpty) {
            emit(
              MainGetRecentReminderSuccessState(
                list: listData,
              ),
            );
            return;
          }

          emit(MainErrorState(message: AppConstants.noData));
        },
      );
    } catch (e) {
      emit(
        MainErrorState(message: AppConstants.noData),
      );
    }
  }

  FutureOr<void> updateReminder(
      MainUpdateReminderEvent event, Emitter<MainState> emit) async {
    emit(MainLoadingState());
    try {
      await _addReminderUseCase.execute(event.reminderModel);
      NotificationUtils().cancelNotif(event.reminderModel.id);
      await Future.delayed(Durations.short1);
      var res = await _getListReminderUseCase.execute();
      res.fold(
        (failure) => emit(MainErrorState(message: failure.message ?? "")),
        (data) {
          if (data
              .where((data) => !data.isActivated && !data.isDeleted!)
              .toList()
              .isNotEmpty) {
            emit(
              MainGetRecentReminderSuccessState(
                list: data
                    .where((data) => !data.isActivated && !data.isDeleted!)
                    .toList()
                    .reversed
                    .toList(),
              ),
            );
            return;
          }

          emit(MainErrorState(message: AppConstants.noData));
        },
      );
    } catch (e) {
      emit(
        MainErrorState(message: AppConstants.noData),
      );
    }
  }
}
