part of 'main_bloc.dart';

@immutable
sealed class MainState extends Equatable {
  @override
  List<Object> get props => [];
}

final class MainInitial extends MainState {}

final class MainLoadingState extends MainState {}

final class MainGetRecentReminderSuccessState extends MainState {
  final List<Reminder> list;

  MainGetRecentReminderSuccessState({required this.list});

  @override
  List<Object> get props => [list];
}

final class MainErrorState extends MainState {
  final String message;

  MainErrorState({required this.message});

  @override
  List<Object> get props => [message];
}
