import 'package:flutter/material.dart';
import 'package:reminder_app/core/app_routes.dart';
import 'package:reminder_app/core/common/common_theme.dart';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: CommonTheme.themeData,
      routerConfig: AppRouter().generateRoute(),
    );
  }
}
